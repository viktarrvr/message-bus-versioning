package com.iii.producer.messaging;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

/**
 * Bindable interface with two output channel.
 *
 * @see org.springframework.cloud.stream.messaging.Source
 * @see org.springframework.cloud.stream.annotation.EnableBinding
 */
public interface ProducerSource {
  String USER_TOPIC = "userTopic";
  String CAR_TOPIC = "carTopic";

  @Output(ProducerSource.USER_TOPIC)
  MessageChannel userTopic();

  @Output(ProducerSource.CAR_TOPIC)
  MessageChannel carTopic();
}
