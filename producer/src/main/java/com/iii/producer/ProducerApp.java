package com.iii.producer;

import com.iii.producer.messaging.ProducerSource;
import com.iii.producer.model.CarType;
import com.iii.producer.model.User;
import java.util.Random;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.schema.client.EnableSchemaRegistryClient;
import org.springframework.integration.annotation.InboundChannelAdapter;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.GenericMessage;

@SpringBootApplication
@EnableBinding(ProducerSource.class)
@EnableSchemaRegistryClient
public class ProducerApp {
	private static final Random RANDOM = new Random();
	private static final Logger LOGGER = LoggerFactory.getLogger(ProducerApp.class);

	public static void main(String[] args) {
		SpringApplication.run(ProducerApp.class, args);
	}

	/**
	 * User output channel. Create one message per second.
	 *
	 * @return user message for kafka
	 */
	@InboundChannelAdapter(ProducerSource.USER_TOPIC)
	public Message<User> userOutput() {
		CarType[] types = CarType.values();
		User user = new User("UserName", RANDOM.nextInt(100));
		LOGGER.info(user.toString());
		return new GenericMessage<>(user);
	}

	/**
	 * Car output channel. Create one message per second.
	 *
	 * @return car message for kafka
	 */
//	@InboundChannelAdapter(ProducerSource.CAR_TOPIC)
//	public Message<Car> carOutput() {
//		CarType[] types = CarType.values();
//		Car car = new Car("CarModel", types[RANDOM.nextInt(types.length)]);
//		LOGGER.info(car.toString());
//		return new GenericMessage<>(car);
//	}


//	/**
//	 * AVRO message converter configuration.
//	 * @return arvo message converter
//	 */
//	@Bean
//	@StreamMessageConverter
//	public MessageConverter userMessageConverter() {
//		AvroSchemaMessageConverter converter = new AvroSchemaMessageConverter(new MimeType("application","*+avro"));
////		converter.setSchemaLocation(new ClassPathResource("schemas/User.avro"));
////		ReflectData.get().addLogicalTypeConversion(new Conversions.UUIDConversion());
//		return converter;
//	}
}
