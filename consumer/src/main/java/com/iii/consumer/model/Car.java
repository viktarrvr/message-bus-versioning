package com.iii.consumer.model;

/**
 * Simple POJO class.
 */
public class Car {

  private String model;
  private CarType type;

  public Car() {
  }

  public Car(String model, CarType type) {
    this.model = model;
    this.type = type;
  }

  public String getModel() {
    return model;
  }

  public void setModel(String model) {
    this.model = model;
  }

  public CarType getType() {
    return type;
  }

  public void setType(CarType type) {
    this.type = type;
  }

  @Override
  public String toString() {
    return "Car{" +
        "model='" + model + '\'' +
        ", type=" + type +
        '}';
  }
}
