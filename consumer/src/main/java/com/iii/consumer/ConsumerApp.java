package com.iii.consumer;

import com.iii.consumer.messaging.ConsumerSink;
import com.iii.consumer.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.schema.client.EnableSchemaRegistryClient;
import org.springframework.messaging.Message;

@SpringBootApplication
@EnableBinding(ConsumerSink.class)
@EnableSchemaRegistryClient
public class ConsumerApp {
	private static final Logger LOGGER = LoggerFactory.getLogger(ConsumerApp.class);

	public static void main(String[] args) {
		SpringApplication.run(ConsumerApp.class, args);
	}

	/**
	 * User input channel.
	 *
	 */
	@StreamListener(ConsumerSink.USER_TOPIC)
	public void log(Message<User> message) {
		LOGGER.info(message.getPayload().toString());
	}
//
//	/**
//	 * AVRO message converter configuration.
//	 * @return arvo message converter
//	 */
//	@Bean
//	@StreamMessageConverter
//	public MessageConverter userMessageConverter() {
//		AvroSchemaMessageConverter converter = new AvroSchemaMessageConverter(new MimeType("application","*+avro"));
////		converter.setSchemaLocation(new ClassPathResource("schemas/User.avro"));
////		ReflectData.get().addLogicalTypeConversion(new Conversions.UUIDConversion());
//		return converter;
//	}
}
