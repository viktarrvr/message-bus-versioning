package com.iii.consumer.messaging;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.MessageChannel;

/**
 * Bindable interface with two output channel.
 *
 * @see org.springframework.cloud.stream.messaging.Source
 * @see org.springframework.cloud.stream.annotation.EnableBinding
 */
public interface ConsumerSink {
  String USER_TOPIC = "userTopic";
  String CAR_TOPIC = "carTopic";

  @Input(ConsumerSink.USER_TOPIC)
  MessageChannel userTopic();

  @Input(ConsumerSink.CAR_TOPIC)
  MessageChannel carTopic();
}
